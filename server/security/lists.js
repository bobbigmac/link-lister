var setListEditMeta = function(activity, doc, fields, modifier) {
	activity.meta = fields.join(',');
	return activity;
}

Lists.permit('insert')
	.ifHasRole('basic')
	.setOwnerUser()
	//.log([false, "created"])
	.apply();

Lists.permit('update')
	.ifHasRole('basic')
	.ownerIsLoggedInUser()
	.onlyProps(['public', 'name', 'description'])
	.log([false, "edited", setListEditMeta])
	.apply();

Lists.permit('remove')
	.ownerIsLoggedInUser()
	.log([false, "delete", "deleted"])
	.apply()