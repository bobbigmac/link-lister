
Meteor.publish('my-lists', function() {
	if(this.userId) {
		return Lists.find({ owner: this.userId });
	}
	this.ready();
	return;
});

var getActivitiesCursor = function(listIds, readUntil) {
	if(listIds && listIds instanceof Array && listIds.length) {
		var options = { sort: { created: -1 }, limit: 100 };
		var filter = {
			list: { '$in': listIds },
			subject: { $nin: ['like'] },
			action: { $nin: ['removed'] }
		};
		if(readUntil) {
			filter.created = { $gt: readUntil };
		}
		return Activities.find(filter, options);
	}
};

Meteor.publish('unread-activities-count', function(listIds, readUntil) {
	var activitiesCursor = getActivitiesCursor(listIds, readUntil);
	if(activitiesCursor) {
		var res = Counts.publish(this, 'unread-activities', activitiesCursor);
	}
	this.ready();
	return;
});

Meteor.publish('activities', function(listIds, readUntil) {
	var activitiesCursor = getActivitiesCursor(listIds, readUntil);
	if(activitiesCursor) {
		return activitiesCursor;
	}
	this.ready();
	return;
});

Meteor.publish('my-subscribed-lists', function() {
	if(this.userId) {
		var user = Meteor.users.findOne({ _id: this.userId });
		var profile = user && user.profile;
		var listIds = (profile && profile.subscriptions && profile.subscriptions.map && profile.subscriptions.map(function(view) {
			return view._id;
		}));
		
		if(listIds && listIds instanceof Array && listIds.length > 1) {
			return Lists.find({ _id: { $in: listIds }});
		}
	}
	this.ready();
	return;
});

Meteor.publish('my-recent-lists', function() {
	if(this.userId) {
		var user = Meteor.users.findOne({ _id: this.userId });
		var profile = user && user.profile;
		var listIds = profile && profile.views && profile.views.map && profile.views.map(function(view) {
			return view._id;
		});

		if(listIds && listIds instanceof Array && listIds.length > 1) {
			return Lists.find({ _id: { $in: listIds }});
		}
	}
	this.ready();
	return;
});

Meteor.publish('likables', function() {
	return Likables.find({});
});

Meteor.publish('lists', function() {
	return Lists.find({ public: true });
});

Meteor.publish('list', function(_id) {
	if(this.userId) {
		var pullViews = {
			$pull: {
				'profile.views': { _id: _id }
			}
		};
		
		Meteor.users.update({ _id: this.userId }, pullViews);

		var pushViews = {
			$push: {
				'profile.views': {
					$each: [{ _id: _id, date: (new Date().getTime()) }],
					$slice: -10
				}
			}
		};
		Meteor.users.update({ _id: this.userId }, pushViews);
	}
	return Lists.find({ _id: _id });
});

Meteor.publish('link-titles', function(linkIds) {
	if(linkIds && linkIds instanceof Array) {
		return Links.find({ _id: { '$in': linkIds }}, { fields: { title: 1 }});
	}
	this.ready();
	return;
});

Meteor.publish('list-names', function(listIds) {
	if(listIds && listIds instanceof Array) {
		return Lists.find({ _id: { '$in': listIds }}, { fields: { name: 1/*, image: 1, 'scanned.image': 1, 'scanned.images.0': 1*/ }});
	}
	this.ready();
	return;
});

Meteor.publish('links', function(listId, linkId) {
	var filter =  { list: listId };
	if(linkId) {
		filter._id = linkId;
	}
	return Links.find(filter, {});
});

Meteor.publish('owners', function(ownerIds) {
	if(ownerIds && ownerIds instanceof Array) {
		return Meteor.users.find({ _id: { '$in': ownerIds }}, { fields: { _id: 1, username: 1, 'profile.email_hash': 1 }});
	}
	this.ready();
	return;
});

Meteor.publish('link-comments', function(linkIds) {
	if(linkIds && linkIds instanceof Array) {
		return Comments.find({ link: { '$in': linkIds }}, { sort: { created: -1 }});
	}
	this.ready();
	return;
});