
var likables = [
 	{ code: 'visit', name: 'Visit it', icon: 'eye-open' },
  { code: 'access', name: 'Access', icon: 'map-marker' },
  { code: 'price', name: 'Price', icon: 'gbp' },
  { code: 'inside', name: 'Inside', icon: 'lamp' },
  { code: 'outside', name: 'Outside', icon: 'home' },
  { code: 'neighbourhood', name: 'Neighbourhood', icon: 'object-align-bottom' },
  //{ code: 'space', name: '', icon: '' }
];

//TODO: Remove default parsers (maybe place in json assets and just load in from there)
var parsers = [
	{
		match: ".*\\.?rightmove\\.co.*\\??.*",
		paths: {
			title: { path: 'title', content: true },
			description: { path: '.description .agent-content .sect', content: true },
			image: { path: 'meta[name="twitter:image:src"]', attribute: 'content' },
			images: { path: '.gallery-grid img', attribute: 'src', multi: true },
			address: { path: 'address', content: true },
			price: { path: '#propertyHeaderPrice strong', content: true },
			sold: { path: '.soldstc', content: true, helper: 'contentExists' },
			thumbs: { path: '.js-gallery-thumbnail img', attribute: 'src', multi: true },
			stations: { path: '.sect .stations-list li', content: true, multi: true, helper: 'stationsListToObjects' },
			location: { path: '.js-ga-minimap img', attribute: 'src', helper: 'googleMapsUrlToLatLong' },
		}
	}, {
		match: ".*\\.?zoopla\\.co.*\\??.*",
		paths: {
			title: { path: 'title', content: true },
			description: { path: 'meta[name="description"]', attribute: 'content' },
			image: { path: 'link[rel="image_src"]', attribute: 'href' },
			images: { path: 'meta[property="og:image"]', attribute: 'content', multi: true },
			address: { path: 'meta[property="og:street-address"]', attribute: 'content' },
			price: { path: '.listing-details-price strong', content: true },
			sold: { path: '.status-text-sold', content: true, helper: 'contentExists' },
			thumbs: { path: '.images-thumb img[itemprop="contentUrl"]', attribute: 'src', multi: true },
			stations: { path: '.nearby_stations_schools li', content: true, multi: true, helper: 'stationsListToObjects' },
			location: { path: 'meta[property="og:latitude"],meta[property="og:longitude"]', attribute: 'content', helper: 'parseArrayOfFloats', multi: true },
		}
	}, {
		match: ".*\\.?onthemarket\\.co.*\\??.*",
		paths: {
			title: { path: 'title', content: true },
			description: { path: 'div.description', content: true },
			image: { path: 'meta[property="og:image"]', attribute: 'content' },
			images: { path: '.main-image-wrapper.image-wrapper img[src]', attribute: 'src', multi: true },
			address: { path: '.details-heading p:nth-of-type(2)', content: true },
			price: { path: '.price-data', content: true },
			sold: { path: '.under-offer', content: true, helper: 'contentExists' },
			thumbs: { path: '.thumbwrap.property-image img[src]', attribute: 'src', multi: true },
			//stations: { path: '.nearby_stations_schools li', content: true, multi: true, helper: 'stationsListToObjects' },
			location: { path: '#details-map a img', attribute: 'src', helper: 'googleMapsUrlToLatLong' },
		}
	}, {
		match: ".*\\.?bridgfords\\.co.*\\??.*",
		paths: {
			title: { path: 'title', content: true },
			description: { path: '#DetailsContentContainer .One', content: true },
			image: { path: '#imgMainPhoto', attribute: 'src' },
			images: { path: '#DetailsThumbnailsContainer li img[src]', attribute: 'src', multi: true, helper: 'redirectThumbUrlPropertywide' },
			address: { path: 'div[itemprop="address"]', content: true },
			price: { path: '.generic-price', content: true },
			sold: { path: '.SSTC', content: true, helper: 'contentExists' },
			thumbs: { path: '#DetailsThumbnailsContainer li img[src]', attribute: 'src', multi: true },
			//location: { path: '#details-map a img', attribute: 'src', helper: 'googleMapsUrlToLatLong' },
		}
	}, {
		match: ".*\\.?vebra\\.co.*\\??.*",
		paths: {
			title: { path: 'title', content: true },
			description: { path: '.DetailsDescription', content: true },
			image: { path: '#DetailsImageCol #dtimg img', attribute: 'src' },
			images: { path: '#DetailsImageCol li img[src]', attribute: 'src', multi: true },
			address: { path: '.DetailsAddress', content: true },
			price: { path: '.DetailsPrice', content: true },
			sold: { path: '.PropStatus', content: true, helper: 'contentExists' },
			// thumbs: { path: '#DetailsThumbnailsContainer li img[src]', attribute: 'src', multi: true },
			//location: { path: '#details-map a img', attribute: 'src', helper: 'googleMapsUrlToLatLong' },
		}
	}
];

Meteor.startup(function() {
	parsers.forEach(function(parser) {
		if(parser.match && parser.paths) {
			var savedParser = ScrapeParser.parser(parser.match, parser.paths);
			//console.log(savedParser._id, 'added for', savedParser.match, Object.keys(savedParser.paths).length, 'fields');
		}
	});

	Links.find({
		url: { $exists: true }, 
		scanning: { $exists: false },
		scanned: { $exists: false }
	}).observe({
	  added: function(link) {
	  	console.log('Scanning', link.url);
	  	
	  	Links.update({ _id: link._id }, { $set: {
	  		scanning: true
	  	}});

	  	var scanned = ScrapeParser.get(link.url);
	  	//console.log(link.url, 'scanned', Object.keys(scanned));

	  	Links.update({ _id: link._id }, { $set: {
	  		image: scanned.image,
	  		title: scanned.title,
	  		description: scanned.description,
	  		scanning: false,
	  		scanned: scanned 
	  	}});
	  }
	});

	if(likables.length !== Likables.find().count()) {
		Likables.remove({});

		likables.forEach(function(likable) {
			Likables.insert(likable);
		});
	}
});
