

ScrapeParser.registerHelper('redirectThumbUrlPropertywide', function(arr) {
	if(arr && arr.map) {
		var res = arr.map(function(url) {
			return url.replace(/(^.+)\/\d+\.jpg$/igm, '$1/11.jpg');
		});
		return res;
	}
	return arr;
});

ScrapeParser.registerHelper('parseArrayOfFloats', function(arr) {
	var res = arr && arr.map(function(val) {
		return val && parseFloat(val);
	});
	return res;
});

ScrapeParser.registerHelper('googleMapsUrlToLatLong', function(str) {
	str = str+'';
	var match = /center=([^&]*)/gi.exec(str);
	var pos = (match && match.length > 1 && match[1]) || '';
	
	pos = pos.split(',');
	if(pos && pos.length === 2) {
		pos = pos.map(function(val) {
			return parseFloat(val);
		});
	}

	return pos || false;
});

ScrapeParser.registerHelper('stationsListToObjects', function(arr) {
	var res = arr.map(function(str) {
		
		//rightmove
		var match = /.*?icon-(.*?)-station.*?span>(.*?)<\/span.*?>\((.*?)\)<.*?/gim.exec(str.replace(/\s/gim, ' '));
		match = (match && match.slice(1, 4));

		if(!match) {
			//zoopla
			//TODO: Too messy and not using for now, parse later
			//console.log('need to parse', str);
		}

		if(match) {
			return {
				type: (match[0] === 'national-train' ? 'train' : (match[0] === 'tram' ? 'tram' : 'unknown')),
				station: match[1],
				distance: match[2] && parseFloat(match[2]),
				unit: 'mi'
			};
		} else {
			//console.log('No match in', str);
		}
	});
	//console.log('stationsListToObjects', arr, res);
	return res;
});

ScrapeParser.registerHelper('contentExists', function(node) {
	return (node && node.length > 0 && node !== 'null' && node !== 'undefined' && node !== 'false') || (node.text && node.text());
});