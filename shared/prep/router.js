Router.configure({
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound',
  layoutTemplate: 'layout'
});

var currentUser = {
  ready: function() {
    var user = Meteor.user();
    return (user === null || typeof user !== "undefined");
  }
};

Router.route('/activity', {
	name: 'activities',
	waitOn: function() {
		return [
			currentUser
		];
	},
	data: function() {
		var activitiesCursor = Activities.find({}, { sort: { created: -1 }});

		Tracker.autorun(function () {
			var filter = {};
			var user = Meteor.user();
			var subscriptions = (user && user.profile && user.profile.subscriptions);
			var readUntil = (user && user.profile && user.profile.readUntil);
			
			Session.set('page-title', subscriptions.length+' subscriptions');

			Meteor.subscribe('activities', subscriptions, readUntil);
			Meteor.subscribe('unread-activities-count', subscriptions, readUntil);


			var owners = {};
			var lists = {};
			var links = {};
			activitiesCursor.fetch().forEach(function(activity) {
				if(!lists[activity.list]) {
					lists[activity.list] = true;
				}
				if(!links[activity.link]) {
					links[activity.link] = true;
				}
				if(!owners[activity.user]) {
					owners[activity.user] = true;
				}
			});

			Meteor.subscribe('list-names', Object.keys(lists));
			Meteor.subscribe('link-titles', Object.keys(links));
			Meteor.subscribe('owners', Object.keys(owners));
		});

		return activitiesCursor;
	},
	fastRender: true
});

Router.route('/', {
	name: 'lists',
	waitOn: function () {
		return [
			currentUser,
			Meteor.subscribe('my-lists'),
			Meteor.subscribe('my-recent-lists'),
			Meteor.subscribe('my-subscribed-lists'),
			Meteor.subscribe('lists')
		];
	},
	data: function() {
		//TODO: sort the lists
		var listsCursor = Lists.find();
		var lists = listsCursor.fetch();
		
		Session.set('page-title', lists.length+' lists');

		Tracker.autorun(function () {
			var user = Meteor.user();
			var subscriptions = (user && user.profile && user.profile.subscriptions);
			var readUntil = (user && user.profile && user.profile.readUntil);
			Meteor.subscribe('unread-activities-count', subscriptions, readUntil);

			var owners = {};
			var lists = listsCursor.fetch();
			var listOwners = (lists && lists.map(function(list) { 
				owners[list.owner] = true; 
				return list.owner; 
			}));

			Meteor.subscribe('owners', Object.keys(owners));
		});
		
		return { lists: listsCursor };
	},
  fastRender: true
});

Router.route('/list/:_id/:link?', {
	name: 'list',
	waitOn: function() {
		if(this.params._id) {
			return [
				currentUser,
				Meteor.subscribe('likables'),
				Meteor.subscribe('list', this.params._id),
				Meteor.subscribe('links', this.params._id, this.params.link)
			];
		}
	},
	data: function() {
		var filter = {};
		if(this && this.params && this.params._id) {
			//console.log(this.params._id);
			filter['_id'] = this.params._id;

			var list = Lists.findOne(filter);

			if(list) {
				var sortables = Session.get('sortables');
				var sortPosition = Session.get('sortPosition');
				var options = {};
				if(sortables[sortPosition]) {
					var sort = {};
					sort[sortables[sortPosition].field] = sortables[sortPosition].value;
					options.sort = sort;
				}

				//var options = { sort: { created: -1 }};
				var filter = { list: list._id };
				
				var targetLink = (this.params && this.params.link);
				Session.set('targetLink', targetLink);
				if(this.params.link) {
					filter._id = this.params.link;
				}
				
				var archived = (Session && Session.get('viewArchived'));
				if(!archived && !targetLink) {
					filter.archived = { $ne: true };
				}
				var linkCursor = Links.find(filter, options);
				var links = linkCursor.fetch();

				Tracker.autorun(function () {
					var user = Meteor.user();
					var subscriptions = (user && user.profile && user.profile.subscriptions);
					var readUntil = (user && user.profile && user.profile.readUntil);
					Meteor.subscribe('unread-activities-count', subscriptions, readUntil);

					Session.set('page-title', linkCursor.count() + ' links on ' + (list && list.name));

					var owners = {};
					var listOwner = (list && list.owner);
					if(listOwner) {
						owners[listOwner] = true;
					}

					var links = linkCursor.fetch();
					var linkIds = [];
					var linkOwners = (links && links.map(function(link) {
						owners[link.owner] = true;
						linkIds.push(link._id);
						return link.owner;
					}));

					Meteor.subscribe('owners', Object.keys(owners));
					Meteor.subscribe('link-comments', linkIds);
				});

				Session.set('holdSort', list._id);

				return { list: list, links: linkCursor };
			}
		}
	},
  fastRender: true
});