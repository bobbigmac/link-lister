


Template.addLinkForm.events({
	'submit .add-link-form': function(event, template) {
		event.preventDefault();
		var link = template.find('.add-link-url').value;
		addLinkToList(link, this._id);
		template.find('.add-link-url').value = '';
	}
});

Template.list.events({
	'keydown': function(event, template) {
		if(event.which === 27) {
			Session.set('editing', false);
		}
	},
	'dblclick .cause-edit': function() {
		var userId = Meteor.userId();
		if(((this.list && this.list.owner) === userId) && !Session.equals('editing', this.list._id)) {
			Session.set('editing', this.list._id);
		} else {
			Session.set('editing', false);
		}
	},
	'change .toggle-archived': function(event, template) {
		Session.set('viewArchived', !!!Session.get('viewArchived'));
	},
	'change .toggle-public': function(event, template) {
		Lists.update({ _id: this.list._id }, {
			$set: {
				'public': !this.list['public']
			}
		});
	},
	'keypress .edit-list-name': function(event, template) {
		if(event.which === 13) {
			var val = (template.find('.edit-list-name').value||'').trim();
			if(val) {
				setListField('name', val, this.list._id);
			}
		}
	},
	'keypress .edit-list-description': function(event, template) {
		if(event.which === 13) {
			setListField('description', (template.find('.edit-list-description').value||'').trim(), this.list._id);
		}
	},
	'click .advance-sort': function(event, template) {
		var sortables = Session.get('sortables');
		var pos = Session.get('sortPosition');
		var newPos = (pos >= (sortables.length-1) ? 0 : pos+1);
		Session.set('sortPosition', newPos);
	},
	'click .toggle-subscribe': function(event, template) {
		var user = Meteor.user();
		var listId = this.list._id;
		if(listId && user && user.profile) {
			var subscriptions = user.profile.subscriptions||[];
			if(subscriptions && subscriptions instanceof Array) {
				var adding = (subscriptions.indexOf(listId) === -1);
				var update = {};
				if(adding) {
					update['$addToSet'] = { 'profile.subscriptions': listId };
				} else {
					update['$pull'] = { 'profile.subscriptions': listId };
				}
				Meteor.users.update({ _id: user._id }, update);
			}
		}
	}
});

Template.list.helpers({
	'editing': function() {
		var userId = Meteor.userId();
		return ((this.list && this.list.owner) === userId) && Session.equals('editing', this.list._id);
	},
	'editable': function() {
		var userId = Meteor.userId();
		return ((this.list && this.list.owner) === userId);
	},
	'viewArchived': function() {
		return Session.equals('viewArchived', true);
	},
	sortValue: function() {
		var sortables = Session.get('sortables');
		var sort = Session.get('sortPosition');
		return (sortables[sort] && sortables[sort].value);
	},
	sortName: function() {
		var sortables = Session.get('sortables');
		var sort = Session.get('sortPosition');
		return (sortables[sort] && sortables[sort].name);
	},
	'masonryCap': function() {
		var numLinks = (this && this.links && this.links.count && this.links.count());
		if(numLinks) {
			if(numLinks < 4) {
				return 'masonry-cap-'+numLinks;
			}
		}
	}
});
