
function linkLikes(linkId, code) {
	var link = Links.findOne({ _id: linkId });
	if(link) {
		return link.likes && link.likes[code] || [];
	}
};

Template.likables.helpers({
	likables: function() {
		//console.log('likables', Likables.find().fetch());
		return Likables.find();
	},
	likes: function(link, code) {
		var likes = linkLikes(link, code);
		return likes && likes.length;
	},
	liked: function(link, code) {
		var userId = Meteor.userId();
		if(userId) {
			var likes = linkLikes(link, code);
			return (likes && likes.indexOf && likes.indexOf(userId) > -1);
		}
	}
});

Template.likables.events({
	'click .likable-link': function(event, template) {
		var userId = Meteor.userId();
		var context = Template.parentData();
		if(context) {
			var link = context.link;
			var code = this.code;
			//console.log(userId, link, this, context.link);

			if(link && code && userId) {
				var likes = linkLikes(link._id, code);
				var liked = likes && likes.indexOf && likes.indexOf(userId) > -1;

				var setProperty = {};
				setProperty['likes.'+code] = userId;

				if(liked) {
					Links.update({ _id: link._id }, {
						$pull: setProperty
					});
				} else {
					Links.update({ _id: link._id }, {
						$addToSet: setProperty
					});
				}
			}
		}
	}
});