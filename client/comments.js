
Template.comments.helpers({
	comments: function() {
		var linkId = this.link && this.link._id;
		return Comments.find({ link: linkId }, { sort: { created: 1 }});
	}
});

// Template.comment.rendered = function() {
// 	var template = Template.instance();
// 	if(template.view.isRendered) {
// 		var linksify = template.$('.linksify');
// 		if(linksify && linksify.length) {
// 			linksify.each(function(pos, el) {
// 				var text = $(el).text();
// 				console.log(text);
// 				var links = getUrlsFromText(this.text);
// 				if(links && links.length) {
// 					links.forEach(function(link, pos) {
// 						var pretty = prettifyLink(link);
// 						text = text.replace(link, '<a href="'+link+'" target="_blank">'+pretty+'</a>');
// 					});
// 				}
// 				console.log('linksify', text);
// 			});
// 		}
// 	}
// };

Template.comment.helpers({
	hasLink: function() {
		var links = getUrlsFromText(this.text);
		return (links && links.length);
	},
	hasOneLink: function() {
		var links = getUrlsFromText(this.text);
		//console.log(links);
		return (links && links.length === 1);
	},
	linked: function() {
		splitUrlsFromText(this.text);
	},
	delinked: function() {
		var links = getUrlsFromText(this.text);
		if(links && links.length) {
			var notLinked = getNotUrlsFromText(this.text, links);
			return (notLinked && (notLinked+'').trim()) || this.text;
		}
		return this.text;
	},
	firstLink: function() {
		var links = getUrlsFromText(this.text);
		return (links && links[0]);
	}
});

Template.comment.events({
	'click .remove-comment': function(event, template) {
		Comments.remove({ _id: this._id });
	}
});