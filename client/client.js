/**
 * Set the brand name for this project
 */
Session.set('brand-name', 'House Hunter');

Template.nav.helpers({
	unreadActivities: function() {
		return Counts.get('unread-activities');
	}
});

Template.nav.events({
	'click .new-list': function() {
		Lists.insert({
			name: 'New List: '+moment().format('LL'),
			public: true
		}, function(err, _id) {
			if(!err && _id) {
				Router.go('list', { _id: _id });
			}
		});
	}
});

Avatar.setOptions({
	fallbackType: 'initials',
	gravatarDefault: 'wavatar',
	emailHashProperty: 'profile.email_hash',
});

Template.layout.helpers({
	loggingIn: function() {
		return Meteor.loggingIn();
	}
})

Accounts.ui.config({
	passwordSignupFields: 'USERNAME_AND_EMAIL'
});

// Meteor.logRenders = function() {
//   _.each(Template, function (template, name) {
//   	console.log(template, name, Template);
//   	if(template) {
// 	    var oldRender = template.rendered;
// 	    var counter = 0;

// 	    template.rendered = function () {
// 	      console.log(name, "render count: ", ++counter);
// 	      oldRender && oldRender.apply(this, arguments);
// 	    };
// 	  }
//   });
// }