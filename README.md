# Link Lister

Platform for organising links into lists. Allows collaboration and comments, likes and public/private lists. 

## Currently implemented as: [House Hunter](http://househunter.meteor.com)

Stores lists of links to house listings on property websites. Allows rating, reviews/comments and ordering for your preferred properties.